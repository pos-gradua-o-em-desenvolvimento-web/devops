# Atividade da Matéria DevOps - Pós em Desenvolvimento Web - IFRO Campus de Vilhena
# Projeto de provisionamento de máquinas virtuais com a utilização de IaaC - Infraestructure as a Service
# 1 - O Arquivo instala_dependencias.sh é um script para instalação do VirtualBox com suas chaves e atualizações, e o Vagrant que irá automatizar a criação das Boxes das VM's.
# 2 - O Arquivo desinstala_dependencias.sh desinstala todos os aplicativos e resíduos no sistema do VirtualBox e Vagrant.
# 3 - O arquivo Vagranfile é baseado em Ruby e provisiona o sistema operacional CentOS, com algumas configurações, e chama o script dependencias_CentOS.sh.
# 4 - O arquivo dependencias_CentOS.sh instala o Podman, e a imagem do MariaDB para Podman, e ainda abre a porta :3306, essencial para acessar o Banco de Dados do MariaDB.
# A partir daí é só começar o desenvolvimento!!!
