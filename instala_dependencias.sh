#!/bin/bash
#instala_dependencias.sh
#----Entrando com superusuário----
Login(){
	if [ "$(id -u)" != "0" ]; then
	echo
	echo -n "Coloque a senha de SuperUser aqui!"
	su -c "sh instala_dependencias.sh"
#su - root -c "sh /home/revialc/Área de Trabalho/IFRO Pós/DevOps/VM/instala_dependencias.sh"
	else
	echo "Logado"
	fi
}
#----Atualizando o Sistema----
apt update -y
#----Instalando o Vagrant-----
apt install vagrant -y
#----Instalando o Repositório do VirtualBox----
sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" >> /etc/apt/sources.list.d/virtualbox.list'
#----Instalando a chave do VirtualBox
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | apt-key add -
#----Atualizando o Sistema----
apt update -y
#----Instalando o VirtualBox
apt-get install virtualbox -y
#----Instalando as Dependências do VirtualBox
apt install virtualbox-dkms libelf-dev -y
