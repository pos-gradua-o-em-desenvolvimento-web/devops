#!/usr/bin/env bash

#----Atualizar o sistema----
sudo yum update -y
#-----Instalação do Podman----
sudo yum -y install podman
#----Instalando o MariaDB no Podman----
podman pull docker.io/library/mariadb
podman run --name mariadb-server -p 3306:3306 -e MYSQL_ROOT_PASSWORD=mariadb -d docker.io/library/mariadb
#----Liberando a porta do MariaDB----
iptables -I INPUT -p tcp --dport 3306 -j ACCEPT
