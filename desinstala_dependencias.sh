#!/bin/bash
#----Entrando como Superusuário----
Login(){
        if [ "$(id -u)" != "0" ]; then
        echo
        echo -n "Coloque sua senha de SuperUser aqui!"
        su -c "sh desinstala_dependencias.sh"
#su - root -c "sh /home/revialc/Área de Trabalho/IFRO Pós/DevOps/VM/desinstala_dependencias.sh"
        else
        echo "Logado"
        fi
}
#----Purgando o Virtualbox em todas as suas versões----
apt purge virtualbox* -y
#----Removendo o Vagrant e aceitando todas as mensagens----
rm -rf /opy/vagrant -y
#----Removendo a pasta da raíz do sistema que contêm o Vagrant e aceitando as mensagens----
rm -f /usr/bin/vagrant -y
#----purgando o vagrant do sistema e aceitando todas as mensagens----
apt purge vagrant* -y
#----Executando o autoremove para apagar arquivos desnecessários e aceitando todas as mensagens----
install --autoremove
#----Atualizando o sistema e aceitando todas as mensagens----
apt update -y
